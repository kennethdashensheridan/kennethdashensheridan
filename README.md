# Hi, I'm Ken Sheridan

I am a problem solver and charismatic leader. 

Before starting my career in big tech I was a Meteorologist in the US Marine Corps for 8 years. I enjoy solving problems, and analyzing large datasets and understanding how distributed systems worked (even nature-made ones) has always captured my curiosity.

Within my current role I develop modular, scalable software to benchmark the performance of our enterprise server components and hardware SKUs. Most recently, my production quality software development has been in Go, but I've also completed projects in Python.

Outside my professional role, I develop in Rust. And I’m very knowledgeable on the low-level protocol implementation details of numerous Web3 projects such as; Hedera Hashgraph, Algorand, IoTex, and Cardano.

If you would like to connect or know more about my professional backstory, please send me an InMail message or email me at kennethdashensheridan@gmail.com.


### Programming & Software Technologies 
Go | Rust (basic) | Python | Bash | gRPC & REST | Protocol Buffers | JSON &  YAML Templating | Redfish & iLO & iDRAC 


### Platforms & Ecosystems

NOW Platform | AWS | Cardano Blockchain | Hedera DLT | Red Hat Enterprise Linux | GitLab CI/CD Pipelines | Docker 


### Soft Skills

Team Building | Public Speaking | Staff On-boarding | Product Roadmap Development | Vendor Engagements


### Testing Toolkit

sybench | FIO | stress-ng | iperf3 | gETH | Cardano-CLI & Cardano-node

### Hardware

All-Flash Storage Arrays | NVMe SDD's | 10/40/100 GbE Networking | Rack-mounted Servers | PCIe & FPGA devices

## Social Profiles
Follow me

* @kennethdashensheridan on [LinkedIn](https://www.linkedin.com/in/kennethdashensheridan "LinkedIn Account")
* @kennethdashen on [Twitter](https://twitter.com/kennethdashen "Twitter Account")
